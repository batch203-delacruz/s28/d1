// CRUD Operations

/*		CREATE
	
	Syntax: 
		db.collectionName.insertOne({Object});

	Comparison with JS

		object.object.method({object});

*/

db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact:{
		phone: "87654321",
		email: "janedoe@gmail.com"
	},
	courses: ["CSS", "JavaScript", "Phython"],
	department: "none"
});

db.rooms.insertOne({
	name: "single",
    accommodates: 2,
    price: 1000,
    description: "A simple room with basic necessities",
    rooms_available: 10,
    isAvailable: false
});

db.rooms.insertMany([
	{
		name: "double",
		accomodates: 3,
		price:2000,
		description: "A room fit for a small family going on a vacation",
		rooms_available: 5,
		isAvailable: false,
	},
	{
		name: "queen",
		accomodates: 4,
		price:4000,
		description: "A room with a queen sized bed perfect for a simple getaway",
		rooms_available: 15,
		isAvailable: false,
	},
])

db.users.insertMany([

	{
		firstName: "Stephen",
		lastName: "Hawking",
		age: 76,
		contact:{
			phone: "87654321",
			email: "stephenhawking@gmail.com"
		},
		courses: ["Phython", "React", "PHP"],
		department: "none"
	},
	{
		firstName: "Neil",
		lastName: "Armstrong",
		age: 76,
		contact:{
			phone: "87654321",
			email: "neilarmstrong@gmail.com"
		},
		courses: ["React", "Laravel", "Sass"],
		department: "none"
	}

])


//  READ - Retrieving a document


db.users.find({});

db.users.find({firstName: "Stephen"});

db.users.find({department: "none"});

db.users.find({lastName: "Armstrong" , age: 76});

// UPDATE - Updating documents

db.users.insertOne({
	firstName: "Test",
	lastName: "Test",
	age: 0,
	contact: {
		phone: "00000000",
		email: "test@gmail.com",
	},
	course: [],
	department: "none"
});

db.users.updateOne(
	{firstName: "Test"},
	{
		$set:{
			firstName: "Bill",
			lastName: "Gates",
			age: 65,
			contact: {
				phone: "12345678",
				email: "bill@gmail.com"
			},
			course: ["PHP", "Laravel", "HTML"],
			department: "Operations",
			status: "active"
		}
	}
);


db.users.updateOne(
	{firstName: "Neil"},
	{
		$set:{
			age: 82
		}
	}
);

db.users.updateMany(
	{department: "none"},
	{
		$set: {department: "HR"}
	}
);

db.users.replaceOne(
	{firstName: "Bill"},
	{
		firstName: "Bill",
		lastName: "Gates",
		age: 65,
		contact: {
			phone: "12345678",
			email: "bill@gmail.com"
		},
		courses: ["PHP", "Laravel", "HTML"],
		department: "Operations",
	}

);

db.rooms.updateOne(
	{name: "queen"},
	{
		$set:{
			rooms_available: 0,
		}
	}
);

// DELETE - Removing


db.users.deleteOne({
	firstName: "Test"
});

db.users.deleteMany({
	firstName: "Test"
});
db.users.find({});

db.rooms.deleteMany(
	{rooms_available: 0}
);

db.rooms.deleteOne(
	{name: "single"}
);
db.rooms.find({});


// ADVANCED QUERIES

	// - embedded document
	db.users.find({
		contact: {
			phone: "87654321",
			email: "stephenhawking@gmail.com"
		}
	});

	// - nested field
	db.users.find({
		contact.email : "stephenhawking@gmail.com"
	});

	// - array with exact element

	db.users.find(
		{
			courses: ["CSS", "JavaScript", "Phython"]
		}
	);

	// - disregarding the array element order
	db.users.find(
		{
			courses: {$all: ["JavaScript", "CSS","Phython"]}
		}
	);


	db.users.find(
		{
			courses: {$all: ["Phython"]}
		}
	);

	//  - embedded array

	db.users.insertOne({
		nameArr:[
		{
			nameA: "Juan"
		},
		{
			nameB: "Tamad"
		}
		]
	});

	db.users.find({
		nameArr:{
			nameA: "Juan"
		}
	});